import json
from nltk import word_tokenize, sent_tokenize, FreqDist
from collections import Counter
from textblob_de import TextBlobDE as TextBlob
from germansentiment import SentimentModel

model = SentimentModel()


def syllable_count(word):
    word = word.lower()
    count = 0
    vowels = "aeiouyäüö"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if count == 0:
        count += 1
    return count


def getMetrics():
    metrics = []

    with open('law.json') as f, open("depend.json") as depend:
        law_file = json.load(f)
        d_file = json.load(depend)
        for i, law in enumerate(law_file):
            if i % 50 == 0:
                print(i, "/", len(law_file))
            with open("parsedLaws/" + law["file"].replace(".xml", ".json")) as parsedLaw:
                lawtext = json.load(parsedLaw)["lawtext"]
                lawtextSanitized = lawtext.translate({ord(c): None for c in '!@#$'})
                blob = TextBlob(lawtext)
                tokens = word_tokenize(lawtextSanitized, language='german')
                sentences = sent_tokenize(lawtextSanitized)

                if len(sentences) > 50:
                    bertsentiment = model.predict_sentiment(sentences[0::10])
                else:
                    bertsentiment = model.predict_sentiment(sentences)

                wordLens = [len(w) if w not in ";:_!$%&/()=,.-" else 0 for w in tokens]
                sentLens = [len(w) for w in sentences]

                dependCount = len(d_file["lawDependecies"][law["name"]])
                freq = FreqDist(tokens)

                syl = [syllable_count(token) if token not in ";:_!$%&/()=,.-" else 0 for token in tokens]

                index_max = max(range(len(wordLens)), key=wordLens.__getitem__)
                metrics.append({
                    "name": law["name"],
                    "year": law["date"][0:4],
                    "averageWordLength": sum(wordLens) / len(wordLens),
                    "wordCount": len(tokens),
                    "sentencesCount": len(sentences),
                    "averageSentenceLength": sum(sentLens) / len(sentences),
                    "paragraphSigns": lawtext.count("§"),
                    "dependencyCount": dependCount,
                    "tableCount": law["countTable"],
                    "weggefallen": lawtext.count("(weggefallen)") + law["count_weggefallen"],
                    "wennCount": freq["wenn"],
                    "oderCount": freq["oder"],
                    "undCount": freq["und"],
                    "bisCount": freq["bis"],
                    "longestWord": tokens[index_max],
                    "w>6": sum(i > 6 for i in syl),
                    "syl=1": sum(i == 1 for i in syl),
                    "sylCount": sum(syl),
                    "syl>=3": sum(i > 2 for i in syl),
                    "unterschiedlicheWoerter": len(list(dict(freq))),
                    "sentiment": blob.sentiment.polarity,
                    "sentimentB": sum(map((lambda x: -1 if x == "negative" else (0 if x == "neutral" else 1)),
                                               bertsentiment))/len(bertsentiment),
                    "Adjektive": Counter(elem[1] for elem in blob.tags)["JJ"],
                    "Präpositionen": Counter(elem[1] for elem in blob.tags)["IN"],
                    "abstrakt": sum([t.endswith(('heit', '.ie', 'ik', 'ion', 'ismus', 'ität', 'keit', 'nz', 'tur', 'ung')) for t in tokens])
                })

    with open("metrics.json", 'w', encoding='utf8') as jsonfile:
        json.dump({"metrics": metrics}, jsonfile, ensure_ascii=False)
