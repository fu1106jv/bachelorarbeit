#!/usr/bin/python3
import json
from urllib.request import urlopen
import io
import os
import shutil
import zipfile

import requests

from bs4 import BeautifulSoup as soup


def crawl():
    counter = 1
    for char in "ABCDEFGHIJKLMNOPQRSTUVWYZ123456789":  # no laws with X
        data = {}
        print("Downloading all laws beginning with {}:".format(char))
        client = urlopen(f"https://www.gesetze-im-internet.de/Teilliste_{char}.html")
        html = client.read()
        client.close()
        html = soup(html, "html.parser")
        # select all <p> elements in div id paddingLR12
        linklist = html.select("#paddingLR12")[0].select("p")

        # creating dict of lists for each site
        for link in linklist:
            url = link.a["href"][2:-11]
            titel = link.a.abbr['title']
            gesetz = link.a.text[1:-1]

            # going one link deeper to get BJNR info because xml files are named by their BJNR
            client = urlopen("https://www.gesetze-im-internet.de/{}/index.html".format(url))
            html_0 = client.read()
            client.close()
            html_0 = soup(html_0, "html.parser")
            BJNR = html_0.h2.a['href'][:-5]

            data[gesetz] = [counter, url, titel, BJNR]

            if counter % 10 == 0:
                print("Nr. " + str(counter))
                print(f"Gesetz: {gesetz}")
                print(f"BJNR: {BJNR}, Titel: {titel}")
                print("---------------------------")
            counter += 1

        with open("data/{}.json".format(char), 'w', encoding='utf8') as file:
            json.dump(data, file, ensure_ascii=False)


def downloadLaws(laws):
    for i, law in enumerate(laws):
        print(f'Nr. {i}/{len(laws)}, Law: {law}')
        zip_data = requests.get(f'https://www.gesetze-im-internet.de/{law}/xml.zip')
        try:
            # create zip file from downloaded data
            zip_file = zipfile.ZipFile(io.BytesIO(zip_data.content))
            law_path = os.path.join('laws/', law[0], law)  # create path to unzip
            shutil.rmtree(law_path, ignore_errors=True)  # remove old zip file data from previous download
            os.makedirs(law_path)
            # extract all files in zipfile
            for name in zip_file.namelist():
                zip_file.extract(name, law_path)
        except zipfile.BadZipfile:
            print("{} is bad zip file".format(law))


def getJSON():
    # open all data/*.json files and send all laws to download function
    all_list = []
    for folder in "ABCDEFGHIJKLMNOPQRSTUVWYZ123456789":
        with open("data/{}.json".format(folder)) as json_file:
            data = json.load(json_file)
        for dat in data:
            all_list.append(data[dat][1])
    return all_list

