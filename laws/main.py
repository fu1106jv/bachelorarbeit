#!/usr/bin/python3
import sys

from crawl import crawl, downloadLaws, getJSON
from parse import parse
from metrics import getMetrics
import inquirer

questions = [inquirer.Checkbox('check', message="Wich Actions should be executed?",
                               default=['Crawl', 'Download Laws', 'Parse Laws', 'Get Law Metrics'],
                               choices=['Crawl', 'Download Laws', 'Parse Laws', 'Get Law Metrics'])]
answers = inquirer.prompt(questions)

if answers["check"] is None:
    sys.exit()

if "Crawl" in answers["check"]:
    print("Crawl ...")
    crawl()
if "Download Laws" in answers["check"]:
    print("Download ...")
    downloadLaws(getJSON())
if "Parse Laws" in answers["check"]:
    print("Parse ...")
    parse()
if "Get Law Metrics" in answers["check"]:
    print("Get Metrics ...")
    getMetrics()
