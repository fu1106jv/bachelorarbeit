import html
import json
import os
import re

from lxml import etree


def getit(pathtoxml):
    tree = etree.parse(pathtoxml)
    root = tree.getroot()
    lawtext = ""
    lawlist = []
    count_weggefallen = 0
    countTable = 0
    # filtering paragraphs out of *.xml files and handling some exceptions
    for child in root.xpath(
            'norm[contains(@doknr,"BJNE")]/metadaten/enbez[contains(text(),"Art ") or contains(text(), "§ ")]'):
        para = {}
        # paragraphs start with §, articles with Art.
        if child.text[:3] == 'Art':
            para['Art'] = child.text[4:]
        else:
            para['pa'] = child.text[2:]
        if child.xpath('../titel/text()') == ['(weggefallen)']:
            count_weggefallen += 1
            para['1'] = '(weggefallen)'
            para['0'] = child.text
            lawtext += " " + child.text
        else:
            # getting max number of paragraphs and storing each in list
            content = child.xpath('../../textdaten//Content[1]/P[not(FnR)]')
            for i, child0 in enumerate(content):
                string_para = str(etree.tostring(child0))
                if "<table" in string_para[2:-1]:
                    countTable += 1
                else:
                    lawtext += " " + sanitize(string_para[2:-1])

                para[str(i + 1)] = sanitize(string_para[2:-1]) if not "<table" in string_para[
                                                                                   2:-1] else "***removedTable***"


        lawlist.append(para)

    name = getProperty(root.xpath('*[1]/metadaten/langue'))
    date = getProperty(root.xpath('*[1]/metadaten/ausfertigung-datum'))
    short = getProperty(root.xpath('*[1]/metadaten/amtabk'))
    short2 = getProperty(root.xpath('*[1]/metadaten/jurabk'))
    mid = getProperty(root.xpath('*[1]/metadaten/kurzue'))

    return name, lawlist, date, short, short2, mid, lawtext[1:], count_weggefallen, countTable


def getProperty(path):
    return sanitizeAll(str(etree.tostring(path[0]))[2:-1])[1:-1] if path else ""


# iterating through lawlist
def lawiter(filename, path, skipEmpty):
    [name, lawlist, date, short, short2, mid, lawtext, count_weggefallen, countTable] = getit(path)
    if lawlist == [] and skipEmpty:
        return
    print("ID: ", filename[:-4], path)

    law = {
        "name": name,
        "date": date,
        "short": short,
        "short2": short,
        "mid": mid,
        "lawlist": lawlist,
        "count_weggefallen": count_weggefallen,
        "countTable": countTable,
        "lawtext": lawtext
    }

    with open(f"parsedLaws/{filename[:-4]}.json", 'w', encoding='utf8') as file:
        json.dump(law, file, ensure_ascii=False)

    return {"name": name, "date": date, "short": short, "short2": short2, "mid": mid,
            "count_weggefallen": count_weggefallen, "file": filename, "countTable": countTable}


def sanitize(txt):
    txt = txt.replace(' Type="arabic"', "")
    txt = txt.replace(' Type="alpha"', "")
    txt = txt.replace(' Type="Alpha"', "")
    txt = txt.replace(' Type="Roman"', "")
    txt = txt.replace(' Type="a3-alpha"', "")
    txt = txt.replace(' Type="a-alpha"', "")
    txt = txt.replace(' Type="Dash"', "")
    txt = txt.replace(' typ="D"', "")
    txt = txt.replace(' Font="normal"', "")
    txt = txt.replace(' Font="bold"', "")
    for i in [4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]:
        txt = txt.replace(f' Indent="{i}"', "")
    txt = txt.replace(' Size="normal"', "")
    txt = txt.replace(' typ="A"', "")
    txt = txt.replace(' class="Rec"', "")
    txt = txt.replace(' xml:space="preserve"', "")

    for rem in ["P", "DD", "DL", "DT", "LA", "I", "BR", "NB", "B", "small", "noindex", "ABWFORMAT", "SUB", "SUP", "U",
                "SP", "pre", "Revision", "F", "/Citation"]:
        txt = txt.replace("<" + rem + ">", "")
        txt = txt.replace("</" + rem + ">", "")
        txt = txt.replace("<" + rem + "/>", "")

    return html.unescape(re.sub("<FnR ID.*?>", " ", re.sub("<IMG.*?>", " ", txt)))


def sanitizeAll(txt):
    return html.unescape(re.sub("<.*?>", " ", txt)).replace("\n", " ").replace("\\n", " ")


def parse():
    skipEmpty = input("Skip Empty? [y,n]") == "y"
    laws = []
    for root, dirs, files in os.walk('laws/'):
        for filename in files:
            if filename.endswith(".xml"):
                law = lawiter(filename, f"{root}/{filename}", skipEmpty)
                if law:
                    laws.append(law)

    with open("law.json", 'w', encoding='utf8') as jsonfile:
        json.dump(laws, jsonfile, ensure_ascii=False)
