import json
from nltk import word_tokenize, sent_tokenize, FreqDist
from textblob_de import TextBlobDE as TextBlob
from collections import Counter
import os
from germansentiment import SentimentModel

dir = "Wikipedia2"
model = SentimentModel()


def syllable_count(word):
    word = word.lower()
    count = 0
    vowels = "aeiouyäüö"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if count == 0:
        count += 1
    return count


def getMetrics():
    metrics = []
    for i, filename in enumerate(os.listdir(dir)):
        if filename == ".DS_Store":
            continue
        print(i, "/", len(os.listdir(dir)))
        with open(dir + "/" + filename) as parsedText:
            text = parsedText.read()
            if len(text) == 0:
                continue
            lawtextSanitized = text.translate({ord(c): None for c in '!@#$'})
            blob = TextBlob(text)
            tokens = word_tokenize(lawtextSanitized, language='german')
            sentences = sent_tokenize(lawtextSanitized)

            wordLens = [len(w) if w not in ";:_!$%&/()=,.-" else 0 for w in tokens]
            sentLens = [len(w) for w in sentences]
            freq = FreqDist(tokens)

            #sentiment = sum([TextBlob(s).sentiment.polarity for s in sentences])
            if len(sentences)>50:
                bertsentiment = model.predict_sentiment(sentences[0::10])
            else:
                bertsentiment = model.predict_sentiment(sentences)

            syl = [syllable_count(token) if token not in ";:_!$%&/()=,.-" else 0 for token in tokens]

            try:
                index_max = max(range(len(wordLens)), key=wordLens.__getitem__)
            except ValueError:
                index_max = -1
            metrics.append({
                "name": filename,
                "averageWordLength": sum(wordLens) / len(wordLens),
                "wordCount": len(tokens),
                "sentencesCount": len(sentences),
                "averageSentenceLength": sum(sentLens) / len(sentences),
                "wennCount": freq["wenn"],
                "oderCount": freq["oder"],
                "undCount": freq["und"],
                "bisCount": freq["bis"],
                "longestWord": 0 if index_max == -1 else tokens[index_max],
                "w>6": sum(i > 6 for i in syl),
                "syl=1": sum(i == 1 for i in syl),
                "sylCount": sum(syl),
                "syl>=3": sum(i > 2 for i in syl),
                "unterschiedlicheWoerter": len(list(dict(freq))),
                "sentiment": blob.sentiment.polarity,
                "sentimentB": sum(map((lambda x: -1 if x == "negative" else (0 if x == "neutral" else 1)),
                                               bertsentiment))/len(bertsentiment),
                "Adjektive": Counter(elem[1] for elem in blob.tags)["JJ"],
                "Präpositionen": Counter(elem[1] for elem in blob.tags)["IN"],
                "abstrakt": sum(
                    [t.endswith(('heit', '.ie', 'ik', 'ion', 'ismus', 'ität', 'keit', 'nz', 'tur', 'ung')) for t in
                     tokens])
            })

    with open("metrics" + dir + ".json", 'w', encoding='utf8') as jsonfile:
        json.dump({"metrics": metrics}, jsonfile, ensure_ascii=False)


getMetrics()
