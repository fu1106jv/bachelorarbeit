import pandas as pd
import json

for f in ["metricsAbituraufgaben.json", "metricsZeitung.json", "metricsWikipedia.json"]:
    d = json.load(open(f))
    d = json.dumps(d["metrics"])
    df = pd.read_json(d)
    df.to_csv(f+'.csv', encoding='utf-8-sig', decimal=",", sep=";")
