import sys
import os
from nltk import word_tokenize, sent_tokenize, FreqDist
from textblob_de import TextBlobDE as TextBlob
from collections import Counter
import math
from termcolor import colored
from germansentiment import SentimentModel
import json

circle = u"\u25CF"

with open('indices.json') as f:
    indices = json.load(f)

model = SentimentModel()


def syllable_count(word):
    word = word.lower()
    count = 0
    vowels = "aeiouyäüö"
    if word[0] in vowels:
        count += 1
    for index in range(1, len(word)):
        if word[index] in vowels and word[index - 1] not in vowels:
            count += 1
    if count == 0:
        count += 1
    return count


def get_metrics(path, text):
    if len(text) == 0:
        return None
    lawtextSanitized = text.translate({ord(c): None for c in '!@#$'})
    blob = TextBlob(text)
    tokens = word_tokenize(lawtextSanitized, language='german')
    sentences = sent_tokenize(lawtextSanitized)

    wordLens = [len(w) if w not in ";:_!$%&/()=,.-" else 0 for w in tokens]
    sentLens = [len(w) for w in sentences]
    freq = FreqDist(tokens)
    # sentiment analysis for long texts may take some time ...
    bertsentiment = model.predict_sentiment(sentences[0::2])
    sentiment = sum(map((lambda x: -1 if x == "negative" else (0 if x == "neutral" else 1)),
                                               bertsentiment))/len(bertsentiment)

    syl = [syllable_count(token) if token not in ";:_!$%&/()=,.-" else 0 for token in tokens]
    return {
        "name": path,
        "averageWordLength": sum(wordLens) / len(wordLens),
        "wordCount": len(tokens),
        "sentencesCount": len(sentences),
        "averageSentenceLength": sum(sentLens) / len(sentences),
        "w>6": sum(i > 6 for i in syl),
        "syl=1": sum(i == 1 for i in syl),
        "sylCount": sum(syl),
        "syl>=3": sum(i > 2 for i in syl),
        "unterschiedlicheWoerter": len(list(dict(freq))),
        "sentiment": blob.sentiment.polarity,
        "senitmentPerSentenceSum": sentiment,
        "Adjektive": Counter(elem[1] for elem in blob.tags)["JJ"],
        "Präpositionen": Counter(elem[1] for elem in blob.tags)["IN"]
    }


def calc(obj):
    metrics = {
        "Flesch": 180 - obj["averageSentenceLength"] - 58.8 * obj["sylCount"] / obj["wordCount"],
        "W1": 0.1935 * obj["syl>=3"] / obj["wordCount"] + 0.1672 * obj["averageSentenceLength"] + 0.1297 * obj["w>6"] /
              obj["wordCount"] + 0.0327 * obj["syl=1"] / obj["wordCount"] - 0.875,
        "W2": 0.2007 * obj["syl>=3"] / obj["wordCount"] + 0.1682 * obj["averageSentenceLength"] + 0.1373 * obj["w>6"] /
              obj["wordCount"] - 2.779,
        "W3": 0.2963 * obj["syl>=3"] / obj["wordCount"] + 0.1905 * obj["averageSentenceLength"] - 1.1144,
        "LIX": obj["averageSentenceLength"] + obj["w>6"],
        "FDK": 10 * obj["syl>=3"] / obj["sentencesCount"],
        "GenaueF": 224.6814 - (79.8304 * math.log(obj["averageWordLength"] + 1)) - (
                12.24032 * math.log(obj["averageSentenceLength"] + 1)) - 1.292857 * (
                           obj["Präpositionen"] / obj["wordCount"]),
        "Koeln": 228.42 - (1.58 * obj["averageSentenceLength"] + 21.3 * obj["averageWordLength"] + 1.53 * (
                100 * obj["Adjektive"] / obj["wordCount"])),
        "SMOG": math.sqrt(30 * obj["syl>=3"] / obj["sentencesCount"]) - 2,
        "Sentiment": obj["sentiment"]
    }

    def padString(string, n):
        return string + (" " * (n - len(string)))

    for el in ["Flesch", "W1", "W2", "W3", "LIX", "FDK", "GenaueF", "Koeln", "SMOG", "Sentiment"]:
        print(padString(el + ": ", 15) + "{0:10.4f}".format(metrics[el]) + "    " + (
            (colored(circle, "red") + circle + circle) if metrics[el] < indices[el]["m"] - indices[el]["sd"] else (
                circle + (colored(circle, "yellow") + circle) if metrics[el] < indices[el]["m"] + indices[el][
                    "sd"] else (
                        circle + circle + colored(circle, "green")))))


arg = sys.argv[1]
if not sys.argv[1]:
    pass
if os.path.isfile(arg):
    with open(arg) as parsedText:
        text = parsedText.read()
    calc(get_metrics(arg, text))
else:
    calc(get_metrics(arg, arg))
