import requests

for i in range(40):
    res = requests.get("https://de.wikipedia.org/wiki/Spezial:Zufällig_in_Kategorie/Kategorie:Wikipedia:Exzellent")
    print("GET ", res.url)
    title = res.url.strip().split("/")[-1]
    response = requests.get(f"https://de.wikipedia.org/w/api.php?action=query&prop=extracts&exlimit=1&titles={title}&explaintext=1&formatversion=2&format=json&exsectionformat=plain")

    file = response.json()["query"]["pages"][0]
    try:
        with open(f"Wikipedia2/{title}.txt", "w", encoding='utf8') as text_file:
            text_file.write(file["extract"].replace("\n", " ").replace("\t", " ").replace("Einzelnachweise", ""))
    except Exception:
        print("ERR with " + title)
